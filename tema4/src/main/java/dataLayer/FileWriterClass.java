package dataLayer;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import businessLayer.MenuItem;
import businessLayer.Order;
import businessLayer.Restaurant;

public class FileWriterClass {

	public FileWriterClass(Restaurant rest, Order comanda, int total) {
		FileWriter write = null;
		PrintWriter printer = null;
		try {
			write = new FileWriter("factura" + comanda.getId() + ".txt", false);
			printer = new PrintWriter(write);

			printer.println("Factura cu numarul " + comanda.getId() + " a restaurantului");
			printer.println(comanda.getData());
			printer.println(" ");
			
			ArrayList<MenuItem> lista = rest.getOrders().get(comanda);
			for(MenuItem produs: lista) {
				printer.println(produs);
			}
			printer.println("---------------------");
			printer.println("Total " + total + " lei");
		} catch (Exception eee) {
			JOptionPane.showMessageDialog(null, "Nu am putut scrie in document!", "Failed!", JOptionPane.ERROR_MESSAGE);
		} finally {
			if (printer != null)
				printer.close();
		}
	}
}
