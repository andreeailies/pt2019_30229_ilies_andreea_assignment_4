package dataLayer;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import businessLayer.MenuItem;
import businessLayer.Restaurant;

public class RestaurantSerializator {
	
	private static final String FILENAME = "produsemeniu.dat";

	public static void scriere(Restaurant rest) {
		ArrayList<MenuItem> lista = rest.getProduseMeniu();
		try {
			FileOutputStream fisier = new FileOutputStream(FILENAME);
			ObjectOutputStream out = new ObjectOutputStream(fisier);
			
			for(MenuItem item: lista) {
				out.writeObject(item);
			}
			out.writeObject(null);
			
			out.close();
			fisier.close();
		} catch(IOException excep1) {
			System.out.println("Nu s-a putut serializa!");
			return;
		}
		System.out.println("Succes serializare!");
	}
	
	public static Restaurant citire() {
		Restaurant restaurant;
		ArrayList<MenuItem> lista = new ArrayList<>();
		try {
			FileInputStream fisier = new FileInputStream(FILENAME);
			ObjectInputStream in = new ObjectInputStream(fisier);
		
			MenuItem produs = null;
			while(true) {
				produs = (MenuItem)in.readObject();
				if (produs == null) break;
				lista.add(produs);
				//System.out.println(produs.getNume());
			}
			in.close();
			fisier.close();
		} catch(IOException excep2) {
			System.out.println("Nu s-a putut deserializa!");
			return new Restaurant();
		} catch (ClassNotFoundException e) {
			System.out.println("Nu am depistat clasa!");
			return new Restaurant();
		} finally {
			
		}
		
		restaurant = new Restaurant(lista);
		System.out.println("Succes deserializare!");
		return restaurant;
	}

	
}
