package presentationLayer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import businessLayer.CompositeProduct;
import businessLayer.MenuItem;
import businessLayer.Restaurant;

public class ViewAllPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private int operatie;
	private JFrame fereastraOriginala;
	private String nume;
	private Restaurant rest;

	private JTable tabel;
	private DefaultTableModel model;
	private Vector<String> header = new Vector<>();

	private JButton butonAux = new JButton();
	private JButton back = new JButton("inapoi");

	public ViewAllPanel(Restaurant rest, JFrame frame, int tip, int operatie, String nume) {
		this.nume = nume;
		this.fereastraOriginala = frame;
		this.operatie = operatie;
		this.rest = rest;
		this.setMaximumSize(new Dimension(400, 400));
		this.setVisible(true);
		this.setBackground(Color.yellow);
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		if (tip == 1) {
			// menuItems
			header.removeAllElements();
			header.add("Nume produs");
			Vector<Vector<MenuItem>> vectorItems = new Vector<Vector<MenuItem>>();

			if (rest.getProduseMeniu() != null)
			for (MenuItem produs : rest.getProduseMeniu()) {
				Vector<MenuItem> aux = new Vector<>();
				aux.add(produs);
				vectorItems.add(aux);
			}

			model = new DefaultTableModel(vectorItems, header);
			// add
			if (operatie == 1) {
				butonAux.setText("Creeaza un produs nou din cele selectate");
				butonAux.setAlignmentX(CENTER_ALIGNMENT);
				butonAux.setBackground(Color.white);
				this.add(butonAux);
				this.add(new JLabel(" "));
			}

			if (operatie == 2) {
				butonAux.setText("Sterge produsele selectate");
				butonAux.setAlignmentX(CENTER_ALIGNMENT);
				butonAux.setBackground(Color.white);
				this.add(butonAux);
				this.add(new JLabel(" "));
			}
			
			if (operatie == 3) {
				butonAux.setText("Editeaza produsul selectat");
				butonAux.setAlignmentX(CENTER_ALIGNMENT);
				butonAux.setBackground(Color.white);
				this.add(butonAux);
				this.add(new JLabel(" "));
			}

		} else {
			// orders
		}

		tabel = new JTable(model);

		JScrollPane scroll = new JScrollPane(tabel);
		scroll.setPreferredSize(new Dimension(300, 300));
		scroll.setVisible(true);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		this.add(scroll);

		this.add(new JLabel(" "));
		back.setAlignmentX(CENTER_ALIGNMENT);
		back.setBackground(Color.white);
		this.add(back);

		ascultatori();
	}

	private void ascultatori() {
		back.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				AdminWindow x = (AdminWindow) fereastraOriginala;
				x.remove(ViewAllPanel.this);
				x.getPanouBaza().setVisible(true);
				x.setContentPane(x.getPanouBaza());
			}

		});

		butonAux.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int produseSelectate[] = tabel.getSelectedRows();
				if (produseSelectate.length != 0) {
					if (operatie == 1) {
						CompositeProduct x = new CompositeProduct(nume);
						for (int indice : produseSelectate) {
							x.getIngrediente().add((MenuItem) tabel.getValueAt(indice, 0));
						}
						rest.getProduseMeniu().add(x);

						JOptionPane.showMessageDialog(null, "Produs adaugat la meniu!", "Super!",
								JOptionPane.INFORMATION_MESSAGE);

					}
					
					if (operatie == 2) {
						for (int indice : produseSelectate) {
							rest.getProduseMeniu().remove((MenuItem) tabel.getValueAt(indice, 0));
						}

						JOptionPane.showMessageDialog(null, "Produs sters din meniu!", "Super!",
								JOptionPane.INFORMATION_MESSAGE);
					}
					
					if (operatie == 3) {
						EditProductPanel panouNou = new EditProductPanel(fereastraOriginala, (MenuItem)tabel.getValueAt(produseSelectate[0], 0));
						AdminWindow x = (AdminWindow) fereastraOriginala;
						ViewAllPanel.this.setVisible(false);
						x.setContentPane(panouNou);
					}
				}

				if (operatie != 3) {
					AdminWindow x = (AdminWindow) fereastraOriginala;
					x.remove(ViewAllPanel.this);
					x.getPanouBaza().setVisible(true);
					x.setContentPane(x.getPanouBaza());
				}
				
			}

		});
	}
}
