package presentationLayer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;


import businessLayer.MenuItem;
import businessLayer.Order;
import businessLayer.Restaurant;
import dataLayer.RestaurantSerializator;

public class ChefWindow extends JFrame implements Observer {

	private static final long serialVersionUID = 1L;

	private JPanel panouBaza = new JPanel();
	private JLabel text = new JLabel("<html>");

	private Restaurant rest;
	
	public ChefWindow(Restaurant rest) {
		this.rest = rest;
		this.setLocation(400,0);
		
		this.setTitle("Fereastra Chefului");
		this.setMinimumSize(new Dimension(400, 400));
		this.setMaximumSize(new Dimension(400, 400));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		this.setContentPane(panouBaza);

		text.setText(text.getText() + "Comenzile pe ziua de azi: <br>");
		text.setForeground(new Color(255, 127, 68));
		text.setOpaque(true);
		text.setBackground(new Color(253, 255, 201));

		panouBaza.setBackground(new Color(239, 227, 4));
		
		ascultatori();
	}
	
	private void ascultatori() {
		this.addWindowListener(new java.awt.event.WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent arg0) {
				RestaurantSerializator.scriere(rest);
				System.exit(0);
			}
		});
	}

	@Override
	public void update(Observable obs, Object obiect) {
		panouBaza.removeAll(); 
		Restaurant rest = (Restaurant) obs;

		Order comanda = (Order) obiect;
		String anunt = "Gatesc noua comanda (" + comanda.getId() + ") ce contine: <br>";
		ArrayList<MenuItem> lista = rest.getOrders().get(comanda);
		for (MenuItem produs : lista) {
			anunt += "  " + produs.getNume() + "<br>";
		}
		
		text.setText(text.getText() + anunt);

		JScrollPane scroll = new JScrollPane(text);
		scroll.setPreferredSize(new Dimension(300, 300));
		scroll.setVisible(true);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		panouBaza.add(scroll);
		
		panouBaza.revalidate();
	}

	/**
	 * @return the panouBaza
	 */
	public JPanel getPanouBaza() {
		return panouBaza;
	}

	/**
	 * @param panouBaza the panouBaza to set
	 */
	public void setPanouBaza(JPanel panouBaza) {
		this.panouBaza = panouBaza;
	}
	
	

}
