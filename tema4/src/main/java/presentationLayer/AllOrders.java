package presentationLayer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import businessLayer.MenuItem;
import businessLayer.Order;
import businessLayer.Restaurant;

public class AllOrders extends JPanel {
	
	private static final long serialVersionUID = 1L;

	private JFrame fereastra;

	private JButton back = new JButton("inapoi");

	private JTable tabel;
	private JScrollPane scroll;
	private DefaultTableModel model;
	private Vector<String> header = new Vector<>();

	public AllOrders(JFrame frame, Restaurant rest) {
		this.fereastra = frame;

		this.setMaximumSize(new Dimension(400, 400));
		this.setVisible(true);
		this.setBackground(Color.yellow);
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		header.removeAllElements();
		header.add("Comanda");
		header.add("Componente");
		Vector<Vector<Object>> vectorItems = new Vector<Vector<Object>>();
		HashMap<Order, ArrayList<MenuItem>> hashulet = rest.getOrders();
		
		for (Order comanda: hashulet.keySet()) {
			Vector<Object> aux = new Vector<>();
			aux.add(comanda);
			vectorItems.add(aux);
			for (MenuItem produs : hashulet.get(comanda)) {
				aux = new Vector<>();
				aux.add(null);
				aux.add(produs);
				vectorItems.add(aux);
			}
		}

		model = new DefaultTableModel(vectorItems, header);
		
		tabel = new JTable(model);

		scroll = new JScrollPane(tabel);
		scroll.setPreferredSize(new Dimension(300, 300));
		scroll.setVisible(true);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		this.add(scroll);

		ascultatori();

		
		this.add(back);
		back.setBackground(Color.white);
		back.setAlignmentX(CENTER_ALIGNMENT);
	}

	private void ascultatori() {
		
		back.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				WaiterWindow x = (WaiterWindow) fereastra;
				x.remove(AllOrders.this);
				x.getPanouBaza().setVisible(true);
				x.setContentPane(x.getPanouBaza());
			}
		});
	}
}
