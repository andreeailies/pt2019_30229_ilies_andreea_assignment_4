package presentationLayer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import businessLayer.MenuItem;
import businessLayer.Order;
import businessLayer.Restaurant;

public class ComandaNouaPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private JFrame fereastra;
	private Restaurant rest;
	private Order comanda;

	private JButton selecteaza = new JButton("confirma");

	private JTable tabel;
	private JScrollPane scroll;
	private DefaultTableModel model;
	private Vector<String> header = new Vector<>();

	public ComandaNouaPanel(JFrame frame, Restaurant rest, Order comanda) {
		this.fereastra = frame;
		this.rest = rest;
		this.comanda = comanda;

		this.setMaximumSize(new Dimension(400, 400));
		this.setVisible(true);
		this.setBackground(Color.yellow);
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		header.removeAllElements();
		header.add("Nume produs");
		Vector<Vector<MenuItem>> vectorItems = new Vector<Vector<MenuItem>>();

		if (rest.getProduseMeniu() != null)
		for (MenuItem produs : rest.getProduseMeniu()) {
			Vector<MenuItem> aux = new Vector<>();
			aux.add(produs);
			vectorItems.add(aux);
		}

		model = new DefaultTableModel(vectorItems, header);
		
		tabel = new JTable(model);

		scroll = new JScrollPane(tabel);
		scroll.setPreferredSize(new Dimension(300, 300));
		scroll.setVisible(true);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		this.add(scroll);

		ascultatori();

		
		this.add(selecteaza);
		selecteaza.setBackground(Color.white);
		selecteaza.setAlignmentX(CENTER_ALIGNMENT);
	}

	private void ascultatori() {
		
		selecteaza.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int produseSelectate[] = tabel.getSelectedRows();
				if (produseSelectate.length != 0) {
					ArrayList<MenuItem> lista = new ArrayList<>();
					
					for (int indice : produseSelectate) {
						lista.add((MenuItem) tabel.getValueAt(indice, 0));
					}
					
					rest.adaugaComanda(comanda, lista);
				}
				
				WaiterWindow x = (WaiterWindow) fereastra;
				x.remove(ComandaNouaPanel.this);
				x.getPanouBaza().setVisible(true);
				x.setContentPane(x.getPanouBaza());
			}
		});
	}
}
