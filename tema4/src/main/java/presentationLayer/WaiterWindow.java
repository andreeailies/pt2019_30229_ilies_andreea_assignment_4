package presentationLayer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import businessLayer.MenuItem;
import businessLayer.Order;
import businessLayer.Restaurant;
import dataLayer.FileWriterClass;
import dataLayer.RestaurantSerializator;

public class WaiterWindow extends JFrame {

	private static final long serialVersionUID = 1L;

	private JPanel panouBaza = new JPanel();
	private JPanel panouButoane = new JPanel();
	
	private JButton mese[] = new JButton[9];
	private Order comenziActive[] = new Order[9];
	private JButton viewAll = new JButton("Vizualizeaza toate comenzile");
	
	private JLabel titlu = new JLabel("Mesele ospatarului");
	
	private Restaurant rest;
	
	public WaiterWindow(Restaurant rest) {
		this.rest = rest;
		this.setLocation(800,0);
		this.setTitle("Fereastra Ospatarului");
		this.setMinimumSize(new Dimension(400, 400));
		this.setMaximumSize(new Dimension(400, 400));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		this.setContentPane(panouBaza);

		panouBaza.setBackground(new Color(107, 208, 255));
		
		this.setUpVisual();
		this.ascultatori();
	}
	
	private void setUpVisual() {
		panouBaza.setLayout(new BoxLayout(panouBaza, BoxLayout.Y_AXIS));
		
		viewAll.setMinimumSize(new Dimension(300, 70));
		viewAll.setMaximumSize(new Dimension(300, 70));
		titlu.setForeground(Color.white);
		viewAll.setBackground(new Color(198, 237, 255));
		viewAll.setForeground(Color.black);
		viewAll.setAlignmentX(CENTER_ALIGNMENT);
		titlu.setAlignmentX(CENTER_ALIGNMENT);
		
		panouBaza.add(new JLabel(" "));
		titlu.setFont(new Font("Arial", Font.BOLD,20));
		panouBaza.add(titlu);
		panouBaza.add(new JLabel(" "));
		butoane();
		panouBaza.add(viewAll);
		panouBaza.add(new JLabel(" "));

	}
	
	private void butoane() {
		panouButoane.setBackground(new Color(107, 208, 255));
		panouButoane.setLayout(new GridLayout(3,3));
		this.add(panouButoane);
		for (int i = 0; i < mese.length; i++) {
			mese[i] = new JButton(Integer.toString(i+1));
			panouButoane.add(mese[i]);
			mese[i].setAlignmentX(CENTER_ALIGNMENT);
			mese[i].setBackground(Color.white);
		}
	}
	
	private void ascultatori() {
		// butoane
		for (int i = 0; i < mese.length; i++) {
			final int j = i;
			mese[i].addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					if (mese[j].getBackground().equals(Color.red)) {
						int pret = WaiterWindow.this.computePriceOrder(comenziActive[j]);
						@SuppressWarnings("unused")
						FileWriterClass bill = new FileWriterClass(rest, comenziActive[j], pret);
						mese[j].setBackground(Color.white);
						comenziActive[j] = null;
						JOptionPane.showMessageDialog(null, "Masa " + (j+1) + " a fost eliberata! Nota de plata emisa!", "Super!",
								JOptionPane.INFORMATION_MESSAGE);
					} else {
						mese[j].setBackground(Color.red); 
						JOptionPane.showMessageDialog(null, "Masa " + (j+1) + " a fost ocupata! Alege produsele!", "Super!",
								JOptionPane.INFORMATION_MESSAGE);
						
						panouBaza.setVisible(false);
						comenziActive[j] = new Order();
						ComandaNouaPanel panouNou = new ComandaNouaPanel(WaiterWindow.this, rest, comenziActive[j]);
						WaiterWindow.this.setContentPane(panouNou);
						
					}
				}
				
			});
		}
		
		viewAll.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				panouBaza.setVisible(false);
				AllOrders panouNou = new AllOrders(WaiterWindow.this, rest);
				WaiterWindow.this.setContentPane(panouNou);
			}
			
		});
		
		this.addWindowListener(new java.awt.event.WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent arg0) {
				RestaurantSerializator.scriere(rest);
				System.exit(0);
			}
		});
	}
	
	private int computePriceOrder(Order comanda) {
		int pret = 0;
		ArrayList<MenuItem> lista = rest.getOrders().get(comanda);
		for (MenuItem produs: lista) {
			pret += produs.computePrice();
		}
		return pret;
	}

	/**
	 * @return the panouBaza
	 */
	public JPanel getPanouBaza() {
		return panouBaza;
	}

	/**
	 * @param panouBaza the panouBaza to set
	 */
	public void setPanouBaza(JPanel panouBaza) {
		this.panouBaza = panouBaza;
	}
	
	
}
