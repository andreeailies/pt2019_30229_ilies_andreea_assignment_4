package presentationLayer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import businessLayer.BaseProduct;
import businessLayer.CompositeProduct;
import businessLayer.MenuItem;
import businessLayer.Restaurant;
import businessLayer.RestaurantProcesare;
import dataLayer.RestaurantSerializator;

/**
 * 
 * @invariant isWellFormed()
 *
 */
public class AdminWindow extends JFrame implements RestaurantProcesare {

	private static final long serialVersionUID = 1L;

	private JPanel panouBaza = new JPanel();

	private JButton add = new JButton("Adauga un produs");
	private JButton edit = new JButton("Editeaza un produs");
	private JButton delete = new JButton("Sterge produse");
	private JButton viewAll = new JButton("Vizualizeaza toate produsele");

	private JLabel titlu = new JLabel("Operatii accesibile administratorului");

	private Restaurant restaurant;

	public AdminWindow(Restaurant rest) {
		this.restaurant = rest;
		this.setLocation(0, 0);
		this.setTitle("Fereastra Administratorului");
		this.setMinimumSize(new Dimension(400, 400));
		this.setMaximumSize(new Dimension(400, 400));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		this.setContentPane(panouBaza);

		panouBaza.setBackground(new Color(242, 16, 39));

		this.setUpVisual();
		this.ascultatori();
	}

	private void setUpVisual() {
		panouBaza.setLayout(new BoxLayout(panouBaza, BoxLayout.Y_AXIS));

		add.setMinimumSize(new Dimension(300, 70));
		edit.setMinimumSize(new Dimension(300, 70));
		delete.setMinimumSize(new Dimension(300, 70));
		viewAll.setMinimumSize(new Dimension(300, 70));

		add.setMaximumSize(new Dimension(300, 70));
		edit.setMaximumSize(new Dimension(300, 70));
		delete.setMaximumSize(new Dimension(300, 70));
		viewAll.setMaximumSize(new Dimension(300, 70));

		titlu.setForeground(Color.white);
		add.setBackground(new Color(255, 239, 241));
		edit.setBackground(new Color(255, 239, 241));
		delete.setBackground(new Color(255, 239, 241));
		viewAll.setBackground(new Color(255, 239, 241));

		add.setForeground(Color.black);
		edit.setForeground(Color.black);
		delete.setForeground(Color.black);
		viewAll.setForeground(Color.black);

		add.setAlignmentX(CENTER_ALIGNMENT);
		edit.setAlignmentX(CENTER_ALIGNMENT);
		delete.setAlignmentX(CENTER_ALIGNMENT);
		viewAll.setAlignmentX(CENTER_ALIGNMENT);
		titlu.setAlignmentX(CENTER_ALIGNMENT);

		panouBaza.add(new JLabel(" "));
		titlu.setFont(new Font("Arial", Font.BOLD, 20));
		panouBaza.add(titlu);
		panouBaza.add(new JLabel(" "));
		panouBaza.add(add);
		panouBaza.add(new JLabel(" "));
		panouBaza.add(edit);
		panouBaza.add(new JLabel(" "));
		panouBaza.add(delete);
		panouBaza.add(new JLabel(" "));
		panouBaza.add(viewAll);
		panouBaza.add(new JLabel(" "));

	}

	private void ascultatori() {
		add.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				String[] tipProdus = new String[] { "Produs de baza", "Produs compus" };
				int option = JOptionPane.showOptionDialog(null, "Ce fel de produs vrei sa adaugi?",
						"Adaugare produs nou in meniu", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null,
						tipProdus, tipProdus[0]);

					adaugareMenuItem(option);
			}

		});

		edit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				editareMenuItem();
			}

		});

		delete.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				stergereMenuItem();
			}

		});

		viewAll.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				panouBaza.setVisible(false);
				ViewAllPanel panouNou = new ViewAllPanel(restaurant, AdminWindow.this, 1, 0, null);
				AdminWindow.this.setContentPane(panouNou);
			}

		});
		
		this.addWindowListener(new java.awt.event.WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent arg0) {
				RestaurantSerializator.scriere(restaurant);
				System.exit(0);
			}
		});
	}
	

	/**
	 * @return the panouBaza
	 */
	public JPanel getPanouBaza() {
		return panouBaza;
	}

	/**
	 * @param panouBaza the panouBaza to set
	 */
	public void setPanouBaza(JPanel panouBaza) {
		this.panouBaza = panouBaza;
	}

	@Override
	public void adaugareMenuItem(int option) {
		assert option != JOptionPane.CANCEL_OPTION;
		assert isWellFormed();
		String nume = JOptionPane.showInputDialog(null, "Introdu numele produsului nou", "Nume produs nou",
				JOptionPane.INFORMATION_MESSAGE);
		if (nume == null) {
			JOptionPane.showMessageDialog(null, "Nume invalid!", "Nasol!", JOptionPane.ERROR_MESSAGE);
			return;
		}
		nume = nume.replaceAll("^\\s+", "");
		nume = nume.replaceAll("\\s+$", "");
		if (nume.equals("")) {
			JOptionPane.showMessageDialog(null, "Nume invalid!", "Nasol!", JOptionPane.ERROR_MESSAGE);
			return;
		}
		if (option == 0) {
			int pret = -1;
			String pretS = JOptionPane.showInputDialog(null, "Introdu pretul produsului nou",
					"Pret produs nou", JOptionPane.INFORMATION_MESSAGE);
			pretS = pretS.replaceAll("^\\s+", "");
			pretS = pretS.replaceAll("\\s+$", "");
			try {
				pret = Integer.parseInt(pretS);
				BaseProduct produsNou = new BaseProduct(nume, pret);
				restaurant.addProdus(produsNou);
				
				JOptionPane.showMessageDialog(null, "Produs adaugat la meniu!", "Super!", JOptionPane.INFORMATION_MESSAGE);
			} catch (Exception e1) {
				JOptionPane.showMessageDialog(null, "Pret invalid! Introdu un numar intreg!", "Nasol!", JOptionPane.ERROR_MESSAGE);
			}

		} else {
			// adaugam la arraylist produse din lista actuala de produse
			panouBaza.setVisible(false);
			ViewAllPanel panouNou = new ViewAllPanel(restaurant, AdminWindow.this, 1, 1, nume);
			AdminWindow.this.setContentPane(panouNou);
		}
		
		assert !restaurant.getProduseMeniu().isEmpty();
		assert isWellFormed();
	}

	@Override
	public void stergereMenuItem() {
		assert !restaurant.getProduseMeniu().isEmpty();
		assert isWellFormed();
		panouBaza.setVisible(false);
		ViewAllPanel panouNou = new ViewAllPanel(restaurant, AdminWindow.this, 1, 2, null);
		AdminWindow.this.setContentPane(panouNou);
		assert isWellFormed();
	}

	@Override
	public void editareMenuItem() {
		assert !restaurant.getProduseMeniu().isEmpty();
		assert isWellFormed();
		panouBaza.setVisible(false);
		ViewAllPanel panouNou = new ViewAllPanel(restaurant, AdminWindow.this, 1, 3, null);
		AdminWindow.this.setContentPane(panouNou);
		assert !restaurant.getProduseMeniu().isEmpty();
		assert isWellFormed();
	}

	@Override
	public boolean isWellFormed() {
		for (MenuItem item : restaurant.getProduseMeniu()) {
			if (item == null) return false;
			if ((item instanceof CompositeProduct) && ((CompositeProduct)item).getIngrediente().isEmpty()) {
				return false;
			}
			if (item.getNume().equals("")) return false;
			if (item.computePrice() <= 0) return false;
		}
		
		return true;
	}
	
	
}
