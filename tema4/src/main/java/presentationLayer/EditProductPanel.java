package presentationLayer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import businessLayer.BaseProduct;
import businessLayer.CompositeProduct;
import businessLayer.MenuItem;

public class EditProductPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private MenuItem produs;
	private JFrame fereastra;

	private JScrollPane scroll;
	private JTable tabel;
	private DefaultTableModel model;
	private Vector<String> header = new Vector<>();

	private JButton back = new JButton("inapoi");
	private JButton editeaza = new JButton("confirma editarea numelui");
	private JButton stergeSelectate = new JButton("sterge componentele selectate");

	private JLabel nume = new JLabel("Editeaza numele produsului:");
	private JLabel pret = new JLabel("Editeaza pretul produsului:");

	private JTextField numeNou = new JTextField();
	private JTextField pretNou = new JTextField();

	public EditProductPanel(JFrame frame, MenuItem produs) {
		this.produs = produs;
		this.fereastra = frame;

		this.setMaximumSize(new Dimension(400, 400));
		this.setVisible(true);
		this.setBackground(Color.yellow);
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		ascultatori();

		this.add(new JLabel(" "));
		editeaza.setAlignmentX(CENTER_ALIGNMENT);
		editeaza.setBackground(Color.white);
		this.add(editeaza);
		this.add(new JLabel(" "));

		this.add(nume);
		this.add(numeNou);
		nume.setAlignmentX(CENTER_ALIGNMENT);
		numeNou.setAlignmentX(CENTER_ALIGNMENT);
		numeNou.setText(produs.getNume());
		System.out.println(produs.getNume());

		if (produs instanceof CompositeProduct) {
			header.removeAllElements();
			header.add("Nume componente");
			Vector<Vector<MenuItem>> vectorItems = new Vector<Vector<MenuItem>>();

			if (((CompositeProduct) produs).getIngrediente() != null)
				for (MenuItem item : ((CompositeProduct) produs).getIngrediente()) {
					Vector<MenuItem> aux = new Vector<>();
					aux.add(item);
					vectorItems.add(aux);
				}

			model = new DefaultTableModel(vectorItems, header);

			tabel = new JTable(model);

			scroll = new JScrollPane(tabel);
			scroll.setPreferredSize(new Dimension(300, 300));
			scroll.setVisible(true);
			scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

			this.add(new JLabel(" "));
			this.add(scroll);
			this.add(new JLabel(" "));
			this.add(stergeSelectate);
			this.add(new JLabel(" "));
			stergeSelectate.setAlignmentX(CENTER_ALIGNMENT);
			stergeSelectate.setBackground(Color.white);

		} else {
			editeaza.setText(editeaza.getText() + "/pretului");
			this.add(new JLabel(" "));

			this.add(pret);
			this.add(pretNou);
			pret.setAlignmentX(CENTER_ALIGNMENT);
			pretNou.setAlignmentX(CENTER_ALIGNMENT);
			pretNou.setText(Integer.toString(produs.computePrice()));
		}

		this.add(new JLabel(" "));
		back.setAlignmentX(CENTER_ALIGNMENT);
		back.setBackground(Color.white);
		this.add(back);
	}

	private void ascultatori() {

		back.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				AdminWindow x = (AdminWindow) fereastra;
				x.remove(EditProductPanel.this);
				x.getPanouBaza().setVisible(true);
				x.setContentPane(x.getPanouBaza());
			}

		});

		editeaza.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String numeFinal = numeNou.getText().replaceAll("^\\s+", "");
				numeFinal = numeFinal.replaceAll("\\s+$", "");
				if (numeFinal.equals("")) {
					JOptionPane.showMessageDialog(null, "Nume invalid!", "Nasol!", JOptionPane.ERROR_MESSAGE);
				} else {
					produs.setNume(numeFinal);
				}
				if (produs instanceof BaseProduct) {
					try {
						String pretFinal = pretNou.getText().replaceAll("^\\s+", "");
						pretFinal = pretFinal.replaceAll("\\s+$", "");
						int pretulet = Integer.parseInt(pretFinal);

						((BaseProduct) produs).setPret(pretulet);

						JOptionPane.showMessageDialog(null, "Produs de baza editat!", "Super!",
								JOptionPane.INFORMATION_MESSAGE);

					} catch (Exception eee) {
						JOptionPane.showMessageDialog(null, "Pret invalid!", "Nasol!", JOptionPane.ERROR_MESSAGE);
					}
				} else {
					JOptionPane.showMessageDialog(null, "Produs compus editat!", "Super!",
							JOptionPane.INFORMATION_MESSAGE);

				}
			}

		});

		stergeSelectate.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int produseSelectate[] = tabel.getSelectedRows();
				if (produseSelectate.length != 0) {

					for (int indice : produseSelectate) {
						((CompositeProduct) produs).getIngrediente().remove((MenuItem) tabel.getValueAt(indice, 0));
					}

					Vector<Vector<MenuItem>> vectorItems = new Vector<Vector<MenuItem>>();

					if (((CompositeProduct) produs).getIngrediente() != null)
						for (MenuItem item : ((CompositeProduct) produs).getIngrediente()) {
							Vector<MenuItem> aux = new Vector<>();
							aux.add(item);
							vectorItems.add(aux);
						}

					model.setDataVector(vectorItems, header);

					JOptionPane.showMessageDialog(null, "Componente sterse!", "Super!",
							JOptionPane.INFORMATION_MESSAGE);
				}
			}

		});
	}

}
