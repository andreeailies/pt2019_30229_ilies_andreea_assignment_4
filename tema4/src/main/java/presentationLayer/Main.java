package presentationLayer;

import businessLayer.Restaurant;
import dataLayer.RestaurantSerializator;

public class Main {

	public static void main(String[] args) {
		Restaurant rest = RestaurantSerializator.citire();
		
		@SuppressWarnings("unused")
		ChefWindow x = new ChefWindow(rest);
		@SuppressWarnings("unused")
		AdminWindow y = new AdminWindow(rest);
		@SuppressWarnings("unused")
		WaiterWindow z = new WaiterWindow(rest);
		
		rest.addObserver(x);
	}

}
