package businessLayer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;

public class Restaurant extends Observable{

	private ArrayList<MenuItem> produseMeniu = new ArrayList<>();
	private HashMap<Order, ArrayList<MenuItem>> orders = new HashMap<>();
	
	public Restaurant() {
		
	}
	
	public void adaugaComanda(Order comanda, ArrayList<MenuItem> lista) {
		orders.put(comanda,  lista);
		setChanged();
		notifyObservers(comanda);
	}
	
	public Restaurant(ArrayList<MenuItem> produse) {
		this.produseMeniu = produse;
	}

	public void addProdus(MenuItem nou) {
		produseMeniu.add(nou);
	}

	public ArrayList<MenuItem> getProduseMeniu() {
		return produseMeniu;
	}

	public void setProduseMeniu(ArrayList<MenuItem> produseMeniu) {
		this.produseMeniu = produseMeniu;
	}

	public HashMap<Order, ArrayList<MenuItem>> getOrders() {
		return orders;
	}

	public void setOrders(HashMap<Order, ArrayList<MenuItem>> orders) {
		this.orders = orders;
	}
	
}
