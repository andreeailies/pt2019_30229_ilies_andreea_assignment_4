package businessLayer;

public class BaseProduct extends MenuItem{

	private static final long serialVersionUID = 1L;
	
	private int pret;
	
	public BaseProduct(String nume, int pret) {
		super(nume);
		this.pret = pret;
	}
	
	@Override
	public int computePrice() {
		return this.pret;
	}
	
	public String toString() {
		return getNume() + " (" + this.computePrice() + " lei)";
	}

	public int getPret() {
		return pret;
	}

	public void setPret(int pret) {
		this.pret = pret;
	}

}
