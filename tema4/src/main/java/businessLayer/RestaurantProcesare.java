package businessLayer;

public interface RestaurantProcesare {

	/**
	 * Adauga la meniu un nou produs.
	 * @pre option != JOptionPane.CLOSED_OPTION
	 * @post !restaurant.getProduseMeniu().isEmpty();
	 */
	public void adaugareMenuItem(int option);
	
	/**
	 * @pre !restaurant.getProduseMeniu().isEmpty();
	 * @post @nochange
	 */
	public void stergereMenuItem();
	
	/**
	 * @pre !restaurant.getProduseMeniu().isEmpty();
	 * @post !restaurant.getProduseMeniu().isEmpty();
	 */
	public void editareMenuItem();
	
	public boolean isWellFormed();
}
