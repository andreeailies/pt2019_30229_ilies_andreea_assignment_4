package businessLayer;

import java.io.Serializable;

public abstract class MenuItem implements Serializable{

	private static final long serialVersionUID = 1L;

	private String nume;
	
	public MenuItem(String nume) {
		this.nume = nume;
	}
	
	public abstract int computePrice();
	
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	
}
