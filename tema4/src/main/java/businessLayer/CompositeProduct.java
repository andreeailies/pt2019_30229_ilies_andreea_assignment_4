package businessLayer;

import java.util.ArrayList;

public class CompositeProduct extends MenuItem{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CompositeProduct(String nume) {
		super(nume);
	}

	private ArrayList<MenuItem> ingrediente = new ArrayList<>();
	
	@Override
	public int computePrice() {
		int pret = 0;
		for (MenuItem ingr: ingrediente) {
			pret+= ingr.computePrice();
		}
		return pret;
	}

	public ArrayList<MenuItem> getIngrediente() {
		return ingrediente;
	}

	public void setIngrediente(ArrayList<MenuItem> ingrediente) {
		this.ingrediente = ingrediente;
	}

	public String toString() {
		return getNume() + " (" + this.computePrice() + " lei)";
	}
}
